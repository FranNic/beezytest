﻿namespace Managers
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DTOs;
    using Database;
    using TMDbLib.Client;
    using TMDbLib.Objects.Discover;
    using TMDbLib.Objects.General;
    using TMDbLib.Objects.Search;
    using Microsoft.EntityFrameworkCore;
    using Database.ModelExtensions;

    public class TheatreManagerManager
    {
        private const int ResultsPerPageFromTMDb = 20;
        private TMDbClient TMDbClient;
        private beezycinemaContext beezycinemaContext;

        // Include lazy init of genres
        public TheatreManagerManager(beezycinemaContext beezycinemaContext)
        {
            this.beezycinemaContext = beezycinemaContext;
            this.TMDbClient = new TMDbClient("1b3b7477e6a45bfdfc47371d16b4c870")
            {
                DefaultLanguage = "en-US"
            };
        }

        public async Task SearchUpcomingRecommendation(string query, int page)
        {
            var results = await this.TMDbClient.GetMovieUpcomingListAsync(this.TMDbClient.DefaultLanguage, page);

            return;
        }

        public async Task<BillboardDto> Billboard(DateTime startDate, int weeksNumber) => throw new NotImplementedException();

        public async Task<BillboardDto> IntelligentBillboard(int bigScreens, int smallScreens, DateTime startDate, int weeksNumber, bool includeSuccesfulMovies)
        {
            var billboard = new BillboardDto();
            var succesfullMovies = new List<MovieDto>();

            // how many movies for each screen
            var numberBigScreenMovies = bigScreens * weeksNumber;
            var numberSmallScreenMovies = smallScreens * weeksNumber;

            // retrieve movies
            var bigRoomMovies = await this.DiscoverMovies(numberBigScreenMovies, DiscoverMovieSortBy.RevenueDesc);
            var smallRoomMovies = await this.DiscoverMovies(numberSmallScreenMovies, DiscoverMovieSortBy.Revenue);
            if (includeSuccesfulMovies)
            {
                var list = await this.GetSuccesfulMovies(numberBigScreenMovies); // Assuming it is only for big screens 
                if (list.Any())
                {
                    succesfullMovies.AddRange(list);
                }
            }

            // build Billboard
            for (int i = 0; i < weeksNumber; i++)
            {
                billboard.Weeks.Add(
                    new BillboardDto.BillboardWeek
                    {
                        BigRoomMovies = bigRoomMovies.Skip(i * bigScreens).Take(bigScreens).ToList(),
                        SmallRoomMovies = smallRoomMovies.Skip(i * smallScreens).Take(smallScreens).ToList(), // hypothetical: "minority genre" are low revenue ones,
                        SuccesfulMovies = includeSuccesfulMovies ? succesfullMovies.Skip(i * bigScreens).Take(bigScreens).ToList() : succesfullMovies,
                        StartDate = startDate.AddDays(7 * i),
                        EndDate = startDate.AddDays(7 + 7 * i),
                    });
            }

            return billboard;
        }

        public async Task<List<MovieDto>> DiscoverMovies(int moviesNeeded, DiscoverMovieSortBy revenueDesc)
        {
            SearchContainer<SearchMovie> searchContainer = new SearchContainer<SearchMovie>
            {
                TotalPages = moviesNeeded / ResultsPerPageFromTMDb,
                Results = new List<SearchMovie>()
            };
            for (int i = 0; i <= searchContainer.TotalPages; i++)
            {
                var movies = await this.TMDbClient.DiscoverMoviesAsync().OrderBy(revenueDesc).Query(i);
                searchContainer.Results.AddRange(movies.Results);
                searchContainer.TotalResults += movies.TotalResults;
            }

            if (searchContainer.TotalResults > 0)
            {
                return searchContainer.Results.Select(x => new MovieDto
                {
                    Title = x.Title,
                    Overview = x.Overview,
                    ReleaseDate = x.ReleaseDate
                }).ToList();
            }

            return new List<MovieDto>();
        }

        private async Task<List<MovieDto>> GetSuccesfulMovies(int take)
        {
            var movies = await this.beezycinemaContext.Movie.Include(m => m.Session)
                                                        .Select(x => new
                                                        {
                                                            Movie = x,
                                                            TotalSeats = x.Session.Sum(s => s.SeatsSold)
                                                        })
                                                        .OrderByDescending(x => x.TotalSeats)
                                                            .Select(x => x.Movie)
                                                                .Take(take)
                                                                    .ToListAsync();

            return movies.Select(x => x.ToDto()).ToList();
        }
    }
}
