﻿namespace Managers
{
    using System;
    using Database;
    using DTOs;
    using TMDbLib;
    using TMDbLib.Client;
    using TMDbLib.Objects.Movies;
    using Microsoft.Extensions.Configuration;
    using TMDbLib.Objects.Search;
    using TMDbLib.Objects.General;
    using System.Threading.Tasks;
    using TMDbLib.Objects.Discover;

    public class ViewerManager
    {
        private TMDbClient TMDbClient;
        
        public ViewerManager()
        {
            this.TMDbClient = new TMDbClient("1b3b7477e6a45bfdfc47371d16b4c870")
            {
                DefaultLanguage = "en-US"
            };
        }

        public async Task<MovieRecommendationDto> SearchMoviesAsync(string query, int page)
        {
            var results = await this.TMDbClient.SearchMovieAsync(query, page);

            return this.MapToDto(results);
        }

        public async Task<TvShowRecommendationDto> SearchTvShowAsync(string query, int page)
        {
            var results = await this.TMDbClient.SearchTvShowAsync(query, page);

            return this.MapToDto(results);
        }

        public async Task<DocumentaryRecommendationDto> SearchDocumentaryAsync(string query, int page)
        {
            var results = this.TMDbClient.DiscoverMoviesAsync().IncludeWithAnyOfGenre(new int[] { 99 });

            return this.MapToDto(results);
        }

        public async Task<MovieRecommendationDto> SearchUpcomingRecommendation(string query, int page)
        {
            var results = await this.TMDbClient.GetMovieUpcomingListAsync(this.TMDbClient.DefaultLanguage, page);

            return this.MapToDto(results);
        }

        // Mappers
        private TvShowRecommendationDto MapToDto(SearchContainer<SearchTv> results) => throw new NotImplementedException();
        private MovieRecommendationDto MapToDto(SearchContainer<SearchMovie> results) => throw new NotImplementedException();
        private DocumentaryRecommendationDto MapToDto(DiscoverMovie results) => throw new NotImplementedException();

    }
}
