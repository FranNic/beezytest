﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using DTOs;

namespace Database.ModelExtensions
{
    public static class MovieModelExtension
    {
        // Basic mapper
        public static MovieDto ToDto(this Movie movie)
        {
            if (movie == null) return null;

            return new MovieDto
            {
                OriginalLanguage = movie.OriginalLanguage,
                ReleaseDate = movie.ReleaseDate,
                Title = movie.OriginalTitle,

            };
        }

    }
}
