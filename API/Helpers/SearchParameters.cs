﻿namespace API.Helpers
{
    public class SearchParameters
    {
        public SearchParameters()
        {
            Page = 1;
            PageSize = 20;
            Query = string.Empty;
        }

        public int Page { get; set; }

        public int? PageSize { get; set; }

        public string Query { get; set; }
    }
}
