﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Helpers
{
    public class RecommendationRequest : SearchParameters
    {
        public RecommendationRequest() : base() 
        {
        }

        public string Genre { get; set; }
    }
}
