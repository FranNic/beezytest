﻿namespace API.Helpers
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class BillboardRequest
    {
        public BillboardRequest() : base()
        { 
        }

        public int BigScreens { get; set; }
        public int SmallScreens { get; set; }
        public int Weeks { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime StartDate { get; set; }

        public bool IncludeSuccesfulMovies { get; set; }
    }
}
