﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    public class BaseController<T> : ControllerBase
    {
        internal readonly ILogger<T> logger;
        public BaseController(ILogger<T> logger)
        {
            this.logger = logger;
        }
    }
}