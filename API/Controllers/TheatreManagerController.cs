﻿namespace API.Controllers
{
    using System;
    using System.Globalization;
    using System.Threading.Tasks;
    using API.Helpers;
    using DTOs;
    using Managers;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    [ApiController]
    public class TheatreManagerController : BaseController<TheatreManagerController>
    {
        private readonly TheatreManagerManager theatreManagerManager;
        public TheatreManagerController(ILogger<TheatreManagerController> logger, TheatreManagerManager theatreManagerManager) : base(logger)
        {
            this.theatreManagerManager = theatreManagerManager;
        }


        [HttpGet("UpcomingMovies")]
        public async Task<ActionResult<MovieRecommendationDto>> UpcomingMovieRecomendation([FromQuery]RecommendationRequest recommendationRequest)
        {
            try
            {
                await this.theatreManagerManager.SearchUpcomingRecommendation(recommendationRequest.Query, recommendationRequest.Page);
                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }


        [HttpGet("Billboard")]
        public async Task<ActionResult<BillboardDto>> Billboard([FromQuery]BillboardRequest billboardRequest)
        {
            try
            {
                await this.theatreManagerManager.Billboard(billboardRequest.StartDate, billboardRequest.Weeks);
                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }

        [HttpGet("IntelligentBillboard")]
        public async Task<ActionResult<BillboardDto>> IntelligentBillboard([FromQuery]BillboardRequest billboardRequest)
        {
            try
            {
                var response = await this.theatreManagerManager.IntelligentBillboard(billboardRequest.BigScreens, billboardRequest.SmallScreens, billboardRequest.StartDate, billboardRequest.Weeks, billboardRequest.IncludeSuccesfulMovies);
                return this.Ok(response);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }
    }
}