﻿namespace API.Controllers
{
    using System;
    using System.Threading.Tasks;
    using API.Helpers;
    using DTOs;
    using Managers;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    [ApiController]
    public class ViewerController : BaseController<ViewerController>
    {
        private readonly ViewerManager viewerManager;

        public ViewerController(ILogger<ViewerController> logger, ViewerManager viewerManager)
            : base(logger)
        {
            this.viewerManager = viewerManager;
        }

        [HttpGet("AllTimeMovies")]
        public async Task<ActionResult<MovieRecommendationDto>> AllTimeMovieRecomendation([FromQuery]RecommendationRequest recommendationRequest)
        {
            try
            {
                var response = await this.viewerManager.SearchMoviesAsync(recommendationRequest.Query, recommendationRequest.Page);
                return this.Ok(response);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }

        [HttpGet("UpcomingMovies")]
        public async Task<ActionResult<MovieRecommendationDto>> UpcomingMovieRecomendation([FromQuery]RecommendationRequest recommendationRequest)
        {
            try
            {
                var response = await this.viewerManager.SearchUpcomingRecommendation(recommendationRequest.Query, recommendationRequest.Page);
                return this.Ok(response);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }

        [HttpGet("AllTimeTVShows")]
        public async Task<ActionResult<TvShowRecommendationDto>> AllTimeTVShowRecomendation([FromQuery]RecommendationRequest recommendationRequest)
        {
            try
            {
                var response = await this.viewerManager.SearchTvShowAsync(recommendationRequest.Query, recommendationRequest.Page);

                return this.Ok(response);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }

        [HttpGet("AllTimeDocumentaries")]
        public async Task<ActionResult<DocumentaryRecommendationDto>> AllTimeDocumentaryRecomendation([FromQuery]SearchParameters recommendationRequest)
        {
            try
            {
                var response = await this.viewerManager.SearchDocumentaryAsync(recommendationRequest.Query, recommendationRequest.Page);
                return this.Ok(response);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return this.NotFound();
            }
        }
    }
}