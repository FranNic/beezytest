﻿namespace DTOs
{
    using System;

    public class BillboardBaseDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BigRoomsAvailable { get; set; }
        public int SmallRoomsAvailable { get; set; }
        public int TotalRoomsAvailable { get => BigRoomsAvailable + SmallRoomsAvailable; }
    }
}