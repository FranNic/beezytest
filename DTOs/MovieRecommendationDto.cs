﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs
{
    public class MovieRecommendationDto : RecommendationBaseDto
    {
        public MovieRecommendationDto() : base()
        {
            this.MovieDtos = new List<MovieDto>();
        }

        public List<MovieDto> MovieDtos { get; set; }
    }
}
