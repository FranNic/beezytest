﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs
{
    public class TvShowRecommendationDto : RecommendationBaseDto
    {
        public TvShowRecommendationDto() : base()
        {
            this.TVShows = new List<TVShowDto>();
        }

        public List<TVShowDto> TVShows { get; set; }
    }
}
