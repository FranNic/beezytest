﻿namespace DTOs
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class MovieDto
    {   
        public string Title { get; set; }
        public string Overview { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string OriginalLanguage { get; set; }
        public string SeatsSold { get; set; }

    }
}
