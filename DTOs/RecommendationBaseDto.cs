﻿namespace DTOs
{
    using System;
    using System.Collections.Generic;

    public class RecommendationBaseDto
    {
        public RecommendationBaseDto()
        {
        }

        public string AssociatedKeywords { get; set; }
        public string Genre { get; set; }
        public int GenreId { get; set; }
    }
}