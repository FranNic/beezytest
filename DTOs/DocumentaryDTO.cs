﻿namespace DTOs
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DocumentaryDTO
    {
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Website { get; set; }
    }
}
