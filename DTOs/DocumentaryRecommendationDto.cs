﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs
{
    public class DocumentaryRecommendationDto : RecommendationBaseDto
    {
        private const string DocumentaryGenre = "Documentary";
        private const int DocumentaryGenreId = 99;

        public DocumentaryRecommendationDto() : base()
        {
            this.MovieDtos = new List<MovieDto>();
            this.TVShowDtos = new List<TVShowDto>();
            this.Genre = DocumentaryGenre;
            this.GenreId = DocumentaryGenreId;
        }

        public List<MovieDto> MovieDtos { get; set; }
        public List<TVShowDto> TVShowDtos { get; set; }
    }
}
