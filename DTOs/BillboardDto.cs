﻿namespace DTOs
{
    using System.Collections.Generic;

    public class BillboardDto : BillboardBaseDto
    {
        public List<BillboardWeek> Weeks { get; set; }

        public BillboardDto()
        {
            this.Weeks = new List<BillboardWeek>();
        }


        public class BillboardWeek : BillboardBaseDto
        {
            public BillboardWeek(List<MovieDto> movies)
            {
                this.BigRoomMovies = new List<MovieDto>();
                this.SmallRoomMovies = new List<MovieDto>();
            }
            public BillboardWeek()
            {
                this.BigRoomMovies = new List<MovieDto>();
                this.SmallRoomMovies= new List<MovieDto>();
            }

            public List<MovieDto> BigRoomMovies { get; set; }
            public List<MovieDto> SmallRoomMovies { get; set; }
            public List<MovieDto> SuccesfulMovies{ get; set; }
        }
    }
}