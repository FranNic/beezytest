﻿namespace DTOs
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class TVShowDto
    {
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Website { get; set; }
        public int Seasons{ get; set; }
        public int Episodes{ get; set; }
        public bool HasConcluded { get; set; }

    }
}
